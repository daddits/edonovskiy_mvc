<h1>Create New Item</h1>
<form method="post" action="goods/create">
    <div class="form-group">
        <label for="name">Item Name</label>
        <input type="text" name="title" class="form-control" placeholder="Enter item name"
               value="example_title">
    </div>
    <div class="form-group">
        <label for="thumbnail">Thumbnail</label>
        <input type="text" name="thumbnail" class="form-control" placeholder="Link for thumbnail"
               value="example_thumbnail.jpg">
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="price">Average Price</label>
            <input type="number" name="price" class="form-control" placeholder="Enter average price"
                   value="123">
        </div>
        <div class="form-group col-md-6">
            <label for="owner">Item Owner</label>
            <input type="text" name="owner" class="form-control" placeholder="Enter owner name"
                   value="example_owner">
        </div>
    </div>

    <button type="submit" class="btn btn-primary">Save Item</button>
    <a href="index.php" class="btn btn-success">Back to main page</a>
</form>
