<?php
/**
 * @var $asc_or_desc
 * @var $column
 * @var $up_or_down
 * @var \app\models\Good $result
 */

?>
<h1>Some Shop Index Page</h1>
<?php if (isset($_GET['success']) && $_GET['success'] == 1): ?>
    <div class='alert alert-primary' role='alert'>New Item have been added</div>
<?php elseif (isset($_GET['error']) && $_GET['error'] == 1): ?>
<div class='alert alert-danger' role='alert'><?= $_GET['error'] ?></div>
<?php endif;  ?>
<table class="table">
    <thead>
    <th scope="col">#</th>
    <th scope="col">
        <a href="index.php?column=name&order=<?php echo $asc_or_desc; ?>">Name<i
                    class="fas fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : '';  ?>"></i></a>
    </th>
    <th scope="col">
        <a href="index.php?column=name&order=<?php echo $asc_or_desc; ?>">Thumbnail<i
                    class="fas fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
    </th>
    <th scope="col">
        <a href="index.php?column=name&order=<?php echo $asc_or_desc; ?>">Date Create<i
                    class="fas fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
    </th>
    <th scope="col">
        <a href="index.php?column=name&order=<?php echo $asc_or_desc; ?>">Good owner<i
                    class="fas fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
    </th>
    <th scope="col">
        <a href="index.php?column=name&order=<?php echo $asc_or_desc;  ?>">Reviews Count<i
                    class="fas fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
    </th>
    <th scope="col">
        Reviews
    </th>
    </thead>
    <?php foreach ($result as $good): ?>
    <tr>
        <td><?= $good['id'] ?></td>
        <td><?= $good['name'] ?></td>
        <td>
            <div class="image-thumbnail">
                <img src="/public/images/<?= $good['thumbnail'] ?>" style="width: 50px"
                     alt="<?= $good['thumbnail'] ?>">
            </div>
        </td>
        <td><?= $good['date_created'] ?></td>
        <td>Owner</td>
        <td>Reviews Count</td>
        <td><a href="reviews/index?id=<?= $good['id'] ?>" class="btn btn-success btn-sm view-review">View Reviews</a>
            <a href="reviews/create?id=<?= $good['id'] ?>" class="btn btn-primary btn-sm add-review">Add Reviews</a>
        </td>
    </tr>
    <?php endforeach;  ?>
</table>
<a href="../goods/create" class="btn btn-success">Create New Good</a>