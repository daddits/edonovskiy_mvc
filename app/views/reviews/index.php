<?php
/**
 * @var int $good_id
 * @var array $result
 */
?>
<h1>Reviews of item <?= $_GET['id'] ?></h1>
<a href="goods/index" class="btn btn-success">Back to main page</a>
<a href="review/create?id=<?= $good_id ?>" class="btn btn-primary btn-sm add-review">Add One More Review</a>
<?php if (empty($result)): ?>
    <h3>No review of this item</h3>
<?php else: ?>
    <table class="table">
        <th>Author</th>
        <th>Rate</th>
        <th>Comment</th>
        <th>Date Created</th>
        <?php foreach ($result as $review): ?>
            <tr>
                <td>Author</td>
                <td>Rate</td>
                <td><?= $review['text'] ?></td>
                <td>Date Created</td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
