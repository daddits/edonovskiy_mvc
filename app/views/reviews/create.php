<h1>Create New Review</h1>
<form method="post" action="reviews/create">
    <div class="form-group">
        <label for="thumbnail">Comment</label>
        <input type="text" name="text" class="form-control" placeholder="Comment text"
               value="example_comment">
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <label for="price">Rating</label>
            <br>
            <input type="radio" value="1" name="rate">1<br>
            <input type="radio" value="2" name="rate">2<br>
            <input type="radio" value="3" name="rate">3<br>
            <input type="radio" value="4" name="rate">4<br>
            <input type="radio" value="5" name="rate">5<br>
            <input type="radio" value="6" name="rate">6<br>
            <input type="radio" value="7" name="rate">7<br>
            <input type="radio" value="8" name="rate">8<br>
            <input type="radio" value="9" name="rate">9<br>
        </div>
        <div class="form-group col-md-6">
            <label for="owner">Author</label>
            <input type="text" name="author" class="form-control" placeholder="Author"
                   value="example_author">
        </div>
        <input type="hidden" name="good_id" class="form-control" placeholder="Author"
               value="<?php echo intval($_GET['id']) ?>>">
    </div>

    <button type="submit" class="btn btn-primary">Save Review</button>
    <a href="index.php" class="btn btn-success">Back to main page</a>
</form>
