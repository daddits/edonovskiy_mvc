<?php

namespace app\controllers;

use app\models\Good;
use core\base\Validator;
use core\DatabaseConnector;

/**
 * Class GoodsController
 */
class GoodsController extends BaseController
{
    /**
     * show list of goods
     */
    public function indexAction()
    {
        $columns = array('name', 'thumbnail', 'date_created');
        $column = isset($_GET['column']) && in_array($_GET['column'], $columns) ? $_GET['column'] : $columns[0];
        $sort_order = isset($_GET['order']) && strtolower($_GET['order']) == 'desc' ? 'DESC' : 'ASC';
        $up_or_down = str_replace(array('ASC', 'DESC'), array('up', 'down'), $sort_order);
        $asc_or_desc = $sort_order == 'ASC' ? 'desc' : 'asc';
        $add_class = ' class="highlight"';

        $result = new Good();
        $result = $result->findAll($column, $sort_order);

        $this->view = 'index';
        $this->set(['result' => $result, 'up_or_down' => $up_or_down, 'asc_or_desc' => $asc_or_desc, 'add_class' => $add_class, 'column' => $column]);
    }

    /**
     * Display form for add new good item
     * If request method is post, save to DB and redirect
     */
    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $item = new Good();
            $data = [];
            $data['name'] = $_POST['title'];
            $data['thumbnail'] = $_POST['thumbnail'];
            $data['date_created'] = date("Y-m-d");

            $fields = [];
            $fields['name'] = 'title';
            $fields['thumbnail'] = 'thumbnail';
            $fields['date_created'] = 'date_created';
            $validator = new Validator(DatabaseConnector::getInstance());
            $validator->checkIsImage('thumbnail', $_POST['thumbnail']);
            if (empty($validator->errors)) {
                if ($item->store($fields, $data)) {
                    //@todo make redirect function
                    //success
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                } else {
                    //save errors
                    //@todo make redirect function
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                }
            } else {
                //validate errors
                //@todo make redirect function
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }
        $this->view = 'create';
        $this->set(null);
    }
}