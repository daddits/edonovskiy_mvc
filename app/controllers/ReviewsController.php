<?php

namespace app\controllers;


use app\models\Review;
use core\base\Validator;
use core\DatabaseConnector;

class ReviewsController extends BaseController
{
    /**
     * Display review
     */
    public function indexAction()
    {
        $good_id = $_GET['id'];
        $review = new Review();
        $result = $review->findByValue('good_id', $good_id);
        $this->view = 'index';
        $this->set(['result' => $result, 'good_id' => $good_id]);
    }

    /**
     * Display form for create new review
     * Add new review
     */
    public function createAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $review = new Review();

            $data = [];
            $data['text'] = $_POST['text'];
            $data['rate'] = $_POST['rate'];
            $data['author'] = $_POST['author'];
            $data['good_id'] = intval($_POST['good_id']);
            $data['date_created'] = date("Y-m-d");

            $fields = [];
            $fields['text'] = 'text';
            $fields['rate'] = 'rate';
            $fields['author'] = 'author';
            $fields['good_id'] = 'good_id';
            $fields['date_created'] = 'date_created';

            $validator = new Validator(DatabaseConnector::getInstance());
            $validator->checkRate(intval($_POST['rate']));

            if (empty($validator->errors)) {
                if ($review->store($fields, $data)) {
                    //@todo make redirect function
                    //success
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                } else {
                    //save errors
                    //@todo make redirect function
                    header('Location: ' . $_SERVER['HTTP_REFERER']);
                }
            } else {
                //validate errors
                //@todo make redirect function
                header('Location: ' . $_SERVER['HTTP_REFERER']);
            }
        }
    }
}