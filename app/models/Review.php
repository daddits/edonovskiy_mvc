<?php

namespace app\models;

use core\base\Entity;

/**
 * Class Review
 */
class Review extends Entity
{
    public $table = 'reviews';
}