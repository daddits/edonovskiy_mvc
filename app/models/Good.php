<?php

namespace app\models;

use core\base\Entity;
use core\DatabaseConnector;

class Good extends Entity
{
    public $table = 'goods';

    /**
     * @return array
     */
    public function getGoodsWithUsers()
    {
        //@todo join with reviews
        $sql = "SELECT t1.name as name, t1.thumbnail, t2.number from goods t1, user t2, users_goods t3 where t3.good_id = t1.id and t3.user_id = t2.id";;

        return $this->pdo->query($sql);
    }

    /**
     * @param $id
     * @return array
     */
    public function getAverageRate($id)
    {
        $sql = "SELECT AVG(rate) FROM reviews WHERE good_id = $id";
        return  $this->pdo->query($sql);
    }

}