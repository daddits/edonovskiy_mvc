<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use core\Router;
$query = rtrim($_SERVER['QUERY_STRING'], '/');

define('CORE', dirname(__DIR__) . '/core');
define('ROOT', dirname(__DIR__) . 'index.php/');
define('APP', dirname(__DIR__) . '/app');
spl_autoload_register(function ($class) {
    $file = ROOT . '/' . str_replace('\\', '/', $class) . '.php';
    if (is_file($file)) {
        require_once $file;
    }
});
Router::add('^$', ['controller' => 'GoodsController', 'action' => 'index']);
Router::add('^(?P<controller>[a-z-]+)/?(?P<action>[a-z-]+)?$');

Router::dispatch($query);
