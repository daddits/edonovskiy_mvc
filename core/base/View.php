<?php

namespace core\base;

/**
 * Class View
 */
class View
{
    const DEFAULT_LAYOUT = 'default';
    public $route = [];
    public $layout;
    public $view;

    public function __construct($route, $layout = '', $view = '')
    {
        $this->route = $route;
        $this->layout = $layout ?: self::DEFAULT_LAYOUT;
        $this->view = $view;
    }

    /**
     * @param array $vars
     */
    public function render(array $vars)
    {
        if (is_array($vars)) {
            extract($vars);
        }
        $keyword = preg_split('/(Controller)/', $this->route['controller']);
        $keyword = strtolower($keyword[0]);
        $file_view = APP . "/views/{$keyword}/{$this->view}.php";
        ob_start();
        if (is_file($file_view)) {
            require $file_view;

        } else {
            echo "<p>View <strong>$file_view</strong></b> not found</p>";
        }
        $content = ob_get_clean();
        $file_layout = APP . "/views/layouts/{$this->layout}.php";
        if (is_file($file_layout)) {
            require $file_layout;
        } else {
            echo "<p>Layout <strong>$file_view</strong></b> not found</p>";
        }
    }
}