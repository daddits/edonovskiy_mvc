<?php

namespace core\base;
use core\DatabaseConnector;

abstract class Entity
{
    protected $pdo;
    protected $table;

    public function __construct()
    {
        $this->pdo = DatabaseConnector::getInstance();
    }

    /**
     * @param $sql
     * @return bool
     */
    public function query($sql)
    {
        return $this->pdo->execute($sql);
    }

    /**
     * @param $column
     * @param $sort_order
     * @return array
     */
    public function findAll($column, $sort_order)
    {
        $sql = "SELECT * FROM {$this->table} ORDER BY $column $sort_order";

        return $this->pdo->query($sql);
    }

    /**
     * @param $column
     * @param $sort_order
     * @param $value
     * @return array
     */
    public function findByValue($column, $value)
    {
        $sql = "SELECT * FROM {$this->table} WHERE $column=$value";

        return $this->pdo->query($sql);
    }

    public function store($fields, $data)
    {
        $field_list = '';  //field list string
        $value_list = '';  //value list string
        foreach ($data as $field => $value) {
            $field_list .= $field . ',';
            $value_list .= "'" . $value . "'" . ',';
        }
        $field_list = rtrim($field_list, ',');
        $value_list = rtrim($value_list, ',');
        $sql = "INSERT INTO {$this->table} ({$field_list}) VALUES ($value_list)";
        return $this->pdo->execute($sql);
    }
}