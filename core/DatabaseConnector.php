<?php

namespace core;
use PDO;
require_once "../config.php";

/**
 * Class DatabaseConnector
 */
class DatabaseConnector
{
    protected $pdo;
    protected static $instance;

    protected function __construct()
    {
        $this->pdo = new PDO("mysql:host=" . HOST . ';dbname=' . DBNAME, DBUSER, DBPASSWORD);
    }

    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public static function setCharsetEncoding()
    {
        if (self::$instance == null) {
            self::connect();
        }

        self::$instance->exec(
            "SET NAMES 'utf8';
			SET character_set_connection=utf8;
			SET character_set_client=utf8;
			SET character_set_results=utf8");
    }

    /**
     * @param $sql
     * @return bool
     */
    public function execute($sql)
    {
        $stmt = $this->pdo->prepare($sql);

        return $stmt->execute();
    }

    /**
     * @param $sql
     * @return array
     */
    public function query($sql)
    {
        $stmt = $this->pdo->prepare($sql);
        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        return [];
    }

    /**
     * @param $table
     * @param $column
     * @return mixed
     */
    public function getMaxLength($table, $column)
    {
        $stmt = self::$instance->prepare('SELECT COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH 
                FROM information_schema.columns 
                WHERE table_schema = DATABASE() AND 
                table_name = :table AND COLUMN_NAME = :column');
        $stmt->execute(['table' => $table, 'column' => $column]);
        $column = $stmt->fetch(PDO::FETCH_LAZY);
        return $column['CHARACTER_MAXIMUM_LENGTH'];
    }
}

?>