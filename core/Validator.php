<?php
namespace core\base;
/**
 * Class Validator
 */
class Validator
{
    CONST MAXRATE = 10;
    private $_db;
    public $errors;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    /**
     * @param $name
     * @param $value
     * @return int|string
     */
    public function checkEmpty($name, $value)
    {
        $name = ucfirst(str_replace("_", " ", $name));
        if (empty($value)) {
            return $this->errors[] = "Please enter $name";
        }
        return 0;
    }

    /**
     * @param $name1
     * @param $value1
     * @param $name2
     * @param $value2
     * @return int|string
     */
    public function checkMatch($name1, $value1, $name2, $value2)
    {
        $name1 = ucfirst(str_replace("_", " ", $name1));
        $name2 = ucfirst(str_replace("_", " ", $name2));
        if ($value1 !== $value2) {
            return $this->errors[] = "Your" . $name2 . "don'\t match" . $name1;
        }
        return 0;
    }

    /**
     * @param $name
     * @param $value
     * @param $table
     * @param $column
     * @return int|string
     */
    public function checkMaxLength($name, $value, $table, $column)
    {
        $name1 = ucfirst(str_replace("_", " ", $name));
        $maxlenth = $this->_db->getMaxLength($table, $column);
        if (strlen($value) > $maxlenth) {
            return $this->errors[] = $name . "is too long. Max length is" . $maxlenth . "characters";
        }
        return 0;
    }

    /**
     * @param $name
     * @param $value
     * @param $minlength
     * @return int|string
     */
    public function checkMinLength($name, $value, $minlength)
    {
        $name1 = ucfirst(str_replace("_", " ", $name));
        if (strlen($value) < $minlength) {
            return $this->errors[] = $name . "is too long. Max length is" . $minlength . "characters";
        }
        return 0;
    }

    public function checkIsImage($name, $value)
    {
        $image_extension = ['jpg', 'jpeg', 'png'];
        $afterDot = explode('.', $value);

        if (count($afterDot) == 1 || in_array($afterDot[0], $image_extension)) {
            return $this->errors[] = 'url string is not for image';
        }
        return 0;
    }

    public function checkRate($value)
    {
        if (is_integer($value) && $value < self::MAXRATE) {
            return 0;
        }
        return $this->errors[] = 'wrong rate';
    }
}